@extends('layouts.admin_app')

@section('sidebar')
  @include('layouts.sidebar.admin_app')
@endsection


@section('content')
  <div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
      <h1 class="h3 mb-0 text-gray-800">Element</h1>
    </div>

    <!-- Content Row -->

    <div class="row">

      <!-- Area Chart -->
      <div class="col-xl-12 col-lg-12">
        <div class="card shadow mb-4">
          <!-- Card Header - Dropdown -->
          <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">Titolo</h6>
            <div class="dropdown no-arrow">
              <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
              </a>
              <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                <div class="dropdown-header">Dropdown Header:</div>
                <a class="dropdown-item" href="{{ route('admin.pages.create') }}">Add Section</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#">Something else here</a>
              </div>
            </div>
          </div>
          <!-- Card Body -->
          <div class="card-body">
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">ID</th>
                  <th scope="col">Title</th>
                  <th scope="col">Text</th>
                  <th scope="col">Date</th>
                </tr>
              </thead>
              <tbody>
                  @foreach ($titles as $title)
                    <tr>
                      <td>
                        {{$title->id}}
                      </td>
                      <td>
                        {{$title->title}}
                      </td>
                      <td>
                        {{str_limit($title->text, 10, '...')}}
                      </td>
                      <td>
                        {{$title->created_at}}
                      </td>
                    </tr>
                  @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="row">

      <!-- Area Chart -->
      <div class="col-xl-12 col-lg-12">
        <div class="card shadow mb-4">
          <!-- Card Header - Dropdown -->
          <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">Sottotitolo</h6>
          </div>
          <!-- Card Body -->
          <div class="card-body">
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">ID</th>
                  <th scope="col">Title</th>
                  <th scope="col">Text</th>
                  <th scope="col">Date</th>
                </tr>
              </thead>
              <tbody>
                  @foreach ($subtitles as $subtitle)
                    <tr>
                      <td>
                        {{$subtitle->id}}
                      </td>
                      <td>
                        {{$subtitle->title}}
                      </td>
                      <td>
                        {{str_limit($subtitle->text, 10, '...')}}
                      </td>
                      <td>
                        {{$subtitle->created_at}}
                      </td>
                    </tr>
                  @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <!-- Area Chart -->
      <div class="col-xl-12 col-lg-12">
        <div class="card shadow mb-4">
          <!-- Card Header - Dropdown -->
          <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">Immagine</h6>
          </div>
          <!-- Card Body -->
          <div class="card-body">
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">ID</th>
                  <th scope="col">Title</th>
                  <th scope="col">Date</th>
                </tr>
              </thead>
              <tbody>
                  @foreach ($images as $image)
                    <tr>
                      <td>
                        {{$image->id}}
                      </td>
                      <td>
                        {{$image->title}}
                      </td>
                      <td>
                        {{$image->created_at}}
                      </td>
                    </tr>
                  @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <!-- Area Chart -->
      <div class="col-xl-12 col-lg-12">
        <div class="card shadow mb-4">
          <!-- Card Header - Dropdown -->
          <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">Descrizione</h6>
          </div>
          <!-- Card Body -->
          <div class="card-body">
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">ID</th>
                  <th scope="col">Title</th>
                  <th scope="col">Text</th>
                  <th scope="col">Date</th>
                </tr>
              </thead>
              <tbody>
                  @foreach ($descriptions as $description)
                    <tr>
                      <td>
                        {{$description->id}}
                      </td>
                      <td>
                        {{$description->title}}
                      </td>
                      <td>
                        {{str_limit($description->text, 50, '...')}}
                      </td>
                      <td>
                        {{$description->created_at}}
                      </td>
                    </tr>
                  @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /.container-fluid -->
@endsection
