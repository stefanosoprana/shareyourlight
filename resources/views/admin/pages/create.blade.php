@extends('layouts.admin_app')

@section('sidebar')
  @include('layouts.sidebar.admin_app')
@endsection

@section('content')

  <div class="container">
    <div class="row">
      <div class="col-12">
        <h1>Aggiungi Nuova Pagina</h1>
        <form class="form-group" action="{{ route('admin.pages.store') }}" method="POST" enctype="multipart/form-data">
          @csrf
          <input type="hidden" name="user_id" value={{Auth::user()->id}} >

          {{-- *** PAGE *** --}}

          <div class="form-group">
            <label for="title">Titolo</label>
            <input type="text" name="title" class="form-control">
          </div>
          <div class="form-group" >
            <label for="description">Descrizione</label>
            <textarea name="description" class="form-control" rows="8" cols="80"></textarea>
          </div>

          <div class="form-group">
            <input type="submit" value="Crea Pagina" class="form-control">
          </div>

        </form>
      </div>
    </div>
  </div>

@endsection
