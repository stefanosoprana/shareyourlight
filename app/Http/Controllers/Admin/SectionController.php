<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use App\Section;
use App\Page;
use App\Title;
use App\Subtitle;
use App\Image;
use App\Description;


class SectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
      $sections = Section::where('page_id', $id)->orderBy('updated_at', 'DESC')->get();

      return view('admin.sections.index', compact('sections'));
    }

    /**
     * Display a listing of the element.
     *
     * @return \Illuminate\Http\Response
     */
    public function element($id)
    {
      $titles = Title::where('section_id', $id)->orderBy('updated_at', 'DESC')->get();

      $subtitles = Subtitle::where('section_id', $id)->orderBy('updated_at', 'DESC')->get();

      $images = Image::where('section_id', $id)->orderBy('updated_at', 'DESC')->get();

      $descriptions = Description::where('section_id', $id)->orderBy('updated_at', 'DESC')->get();

      return view('admin.sections.element', compact('titles', 'subtitles', 'images', 'descriptions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Page $page)
    {
        dd($page->id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
